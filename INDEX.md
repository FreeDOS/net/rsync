# rsync

rsync is a utility software that synchronizes files and directories from one location to another while minimizing data transfer by using delta encoding when appropriate. This rsync port is necessarily a sub-set of the full rsync, partly because of memory limitations and partly because of the peculiarities of DOS. The main differences are: Client only ; No support for rsh or ssh ; File names on the server must be in DOS format ; File data is not compressed.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## RSYNC.LSM

<table>
<tr><td>title</td><td>rsync</td></tr>
<tr><td>version</td><td>2.2.5a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-02-01</td></tr>
<tr><td>description</td><td>Synchronizes files and directories</td></tr>
<tr><td>keywords</td><td>rsync</td></tr>
<tr><td>original&nbsp;site</td><td>http://rsync.samba.org</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>rsync is a utility software that synchronizes files and directories from one location to another while minimizing data transfer by using delta encoding when appropriate. This rsync port is necessarily a sub-set of the full rsync, partly because of memory limitations and partly because of the peculiarities of DOS. The main differences are: Client only ; No support for rsh or ssh ; File names on the server must be in DOS format ; File data is not compressed.</td></tr>
</table>
